<?php

use Illuminate\Support\Facades\Route;

use  App\Http\Controllers\Admin\Products\ProductsController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for the admin area.
| Prefix: /admin
| Middleware: auth auth.admin
|
*/

Route::get('/products', [ProductsController::class, 'index'])->name('admin.products');
