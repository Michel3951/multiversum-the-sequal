## Multiversum 2
Because why not?

### Database rules
- Table and column names must use lower case letters or numbers.
- Table and column names may not contain spaces, use underscores instead (_).
- All tables must use the InnoDB engine.
- The collation must be utf8_unicode_ci.
- Use relations where possible.

### Migration rules

- Any foreign must go after the primary key.
- Group types, ex: int int text text bool. Not: int text bool text int.
- Booleans must start with is_, for example "is_admin" rather than "admin".
- Use empty lines when switching from type.
