<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }} - Beheer</title>

    <link rel="icon" href="{{ asset('favicon.png') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">

    @stack('head')
</head>
<body>

<div id="app">

    <div id="sidebar">
        @include('_partials.admin.header')
    </div>

    <div id="content">
        @yield('content')
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
@stack('js')
</body>
</html>
