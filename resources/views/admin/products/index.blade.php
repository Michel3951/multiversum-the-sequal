@extends('_layouts.admin')

@section('content')
    <div class="container-fluid">
        <h2>Producten</h2>

        <div class="card shadow">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Naam</th>
                    <th>Prijs</th>
                    <th>Voorraad</th>
                    <th>Acties</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td><b>{{ $product->id }}</b></td>
                        <td>{{ $product->name }}</td>
                        <td>{!! $product->price_formatted !!}</td>
                        <td>{{ $product->stock ?? 'N/A' }}</td>
                        <td>
                            <i class="mdi mdi-eye-outline"></i>
                            <i class="mdi mdi-pencil-outline ms-2"></i>
                            <i class="mdi mdi-delete-alert ms-2"></i>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
