<div class="vh-100 position-fixed p-3 bg-white h-100 shadow">
    <a href="/admin" class="d-flex pb-3 mb-3 link-dark border-bottom">
        <img class="bi mx-auto" height="70" src="{{ asset('favicon.png') }}" alt="multiversum logo">
    </a>
    <ul class="list-unstyled ps-0">
        <li class="mb-1">
            <button class="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="true">
               <i class="mdi mdi-format-list-text"></i> Producten
            </button>
            <div class="collapse" id="home-collapse">
                <ul class="btn-toggle-nav list-unstyled ms-4 fw-normal pb-1">
                    <li><a href="{{ route('admin.products') }}" class="link-dark rounded">Overzicht</a></li>
                    <li><a href="#" class="link-dark rounded">Korting</a></li>
                </ul>
            </div>
        </li>
    </ul>
</div>
