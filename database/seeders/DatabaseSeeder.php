<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Michel',
            'last_name' => 'Bunschoten',
            'password' => bcrypt('password'),
            'email' => 'me@example.com',
            'is_admin' => true
        ]);

        User::factory(10)
            ->create();

        Product::factory(30)
            ->hasFacts(4)
            ->hasSpecifications(10)
            ->create();
    }
}
