<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'description',
        'image_url'
    ];

    public function specifications(): HasMany
    {
        return $this->hasMany(ProductSpecification::class);
    }

    public function facts(): HasMany
    {
        return $this->hasMany(ProductFact::class);
    }

    public function getPriceFormattedAttribute(): string
    {
        return "&euro; " . number_format($this->getAttribute('price'), 2, ',', '.');
    }
}
